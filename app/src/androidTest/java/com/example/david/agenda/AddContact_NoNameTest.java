package com.example.david.agenda;


import android.support.design.widget.TextInputLayout;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AddContact_NoNameTest {

   @Rule
   public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

   @Test
   public void addContact_NoNameTest() {
      ViewInteraction floatingActionButton = onView(
            allOf(withId(R.id.b_new_contact), isDisplayed()));
      floatingActionButton.perform(click());

      ViewInteraction actionMenuItemView = onView(
            allOf(withId(R.id.b_new_contact), isDisplayed()));
      actionMenuItemView.perform(click());
      // Comprueba que siga en la vista de añadir
      ViewInteraction editText = onView(
            allOf(withId(R.id.et_name),
                  childAtPosition(
                        childAtPosition(
                              IsInstanceOf.<View>instanceOf(TextInputLayout.class),
                              0),
                        0),
                  isDisplayed()));
      editText.check(matches(isDisplayed()));

   }

   private static Matcher<View> childAtPosition(
         final Matcher<View> parentMatcher, final int position) {

      return new TypeSafeMatcher<View>() {
         @Override
         public void describeTo(Description description) {
            description.appendText("Child at position " + position + " in parent ");
            parentMatcher.describeTo(description);
         }

         @Override
         public boolean matchesSafely(View view) {
            ViewParent parent = view.getParent();
            return parent instanceof ViewGroup && parentMatcher.matches(parent)
                  && view.equals(((ViewGroup) parent).getChildAt(position));
         }
      };
   }
}
