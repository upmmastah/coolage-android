package com.example.david.agenda.util;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Fede Bossman on 15/01/2017.
 */
public class ContactDataValidatorTest {

   @Test
   public void validateNameEmpty_EmptyName() {
      String name = "";
      // Test empty string, shouldn't be valid
      assertFalse(ContactDataValidator.validateNameEmpty(name));
   }

   @Test
   public void validateNameEmpty_NonEmptyName() {
      String name = "Federico";
      // Test non empty name, should be valid
      assertTrue(ContactDataValidator.validateNameEmpty(name));
   }

   @Test
   public void validateNameLength_InvalidString() {
      String name = "Da";
      // Test 2 character string, shouldn't be valid
      assertFalse(ContactDataValidator.validateNameLength(name));
   }


   @Test
   public void validateNameLength_ValidString() {
      String name = "Dav";
      // Test 3 character string, should be valid
      assertTrue(ContactDataValidator.validateNameLength(name));
   }

   @Test
   public void validateEmail_NoArroba() {
      String email = "thisisnotanemail.com";
      // Test no arroba
      assertFalse(ContactDataValidator.validateEmail(email));
   }

   @Test
   public void validateEmail_NoExtension() {
      String email = "thisisnotanemail@something";
      // Test no extension
      assertFalse(ContactDataValidator.validateEmail(email));
   }

   @Test
   public void validateEmail_ValidString() {
      String email = "thisisanemail@something.com";
      // Test no extension
      assertTrue(ContactDataValidator.validateEmail(email));
   }
}