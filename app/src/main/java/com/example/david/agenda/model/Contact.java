package com.example.david.agenda.model;

public class Contact {

   private String id;
   private String name;
   private String surname;
   private String phone;
   private String email;
   private String address;
   private boolean favorite;

   public Contact() {
   }

   public Contact(String id, String name, String surname, String phone, String email, String address) {
      this.id = blankToNull(id);
      this.name = blankToNull(name);
      this.surname = blankToNull(surname);
      this.phone = blankToNull(phone);
      this.email = blankToNull(email);
      this.address = blankToNull(address);
      this.favorite = false;
   }

   private static String blankToNull(String s) {
      return s == null || s.isEmpty() ? null : s;
   }


   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getSurname() {
      return surname;
   }

   public void setSurname(String surname) {
      this.surname = surname;
   }

   public String getPhone() {
      return phone;
   }

   public void setPhone(String phone) {
      this.phone = phone;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getAddress() {
      return address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public boolean getFavorite() {
      return favorite;
   }

   public void setFavorite(boolean favorite) {
      this.favorite = favorite;
   }
}
