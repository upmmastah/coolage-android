package com.example.david.agenda.model;

import java.util.List;

/**
 * Created by Fede Bossman on 16/01/2017.
 */

public class ContactList {

   private int count = 0;
   private String next = null;
   private String previous = null;
   private List<Contact> results = null;

   public int getCount() {
      return count;
   }

   public void setCount(int count) {
      this.count = count;
   }

   public String getNext() {
      return next;
   }

   public void setNext(String next) {
      this.next = next;
   }

   public String getPrevious() {
      return previous;
   }

   public void setPrevious(String previous) {
      this.previous = previous;
   }

   public List<Contact> getResults() {
      return results;
   }

   public void setResults(List<Contact> results) {
      this.results = results;
   }
}
