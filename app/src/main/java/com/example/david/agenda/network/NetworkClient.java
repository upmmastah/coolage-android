package com.example.david.agenda.network;

import android.util.Base64;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkClient {

   public static final String BASE_URL = "http://ec2-35-162-6-163.us-west-2.compute.amazonaws.com/";
   private static Retrofit retrofit = null;

   private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

   private static NetworkInterface networkService;

   private static Retrofit.Builder builder =
         new Retrofit.Builder()
               .baseUrl(BASE_URL)
               .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
               .addConverterFactory(GsonConverterFactory.create());

   public static NetworkInterface getService() {
      if (networkService == null) {
         networkService = NetworkClient.createService(NetworkInterface.class, "ubuntu", "albertoparguelas");
      }
      return networkService;
   }

   public static <S> S createService(Class<S> serviceClass) {
      return createService(serviceClass, null, null);
   }

   public static <S> S createService(Class<S> serviceClass, String username, String password) {
      if (username != null && password != null) {
         String credentials = username + ":" + password;
         final String basic =
               "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

         httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
               Request original = chain.request();

               Request.Builder requestBuilder = original.newBuilder()
                     .header("Authorization", basic)
                     .header("Accept", "application/json")
                     .method(original.method(), original.body());

               Request request = requestBuilder.build();
               return chain.proceed(request);
            }
         });
      }

      OkHttpClient client = httpClient.build();
      Retrofit retrofit = builder.client(client).build();
      return retrofit.create(serviceClass);
   }
}
