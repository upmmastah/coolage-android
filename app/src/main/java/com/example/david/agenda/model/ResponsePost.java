package com.example.david.agenda.model;

/**
 * Created by David on 17/11/16.
 */

public class ResponsePost {
   String id;

   public ResponsePost(String id) {
      this.id = id;
   }

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }
}
