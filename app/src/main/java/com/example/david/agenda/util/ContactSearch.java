package com.example.david.agenda.util;

import android.util.Log;

import com.example.david.agenda.model.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 16/1/17.
 */

public class ContactSearch {
    public static List<Contact> searchContacts(List<Contact> contacts, boolean[] searchFilter, String s){
        List<Contact> filteredContacts = new ArrayList<>();

        for (Contact contact:contacts) {
            boolean canAddContact = false;
            Log.d("entra", contact.getName());

            if(searchFilter[0]){
                if(contact.getName().contains(s))
                    canAddContact = true;
            }
            if(searchFilter[1]){
                if(contact.getSurname().contains(s))
                    canAddContact = true;
            }
            if(searchFilter[2]){
                if(contact.getPhone().contains(s))
                    canAddContact = true;
            }
            if(searchFilter[3]){
                if(contact.getEmail().contains(s))
                    canAddContact = true;
            }
            if(searchFilter[4]){
                if(contact.getAddress().contains(s))
                    canAddContact = true;
            }

            if(canAddContact)
                filteredContacts.add(contact);
        }

        return filteredContacts;
    }
}
