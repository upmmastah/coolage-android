package com.example.david.agenda.model;

/**
 * Created by David on 17/11/16.
 */

public class ResponseDjango {

   String status;
   String message;

   public ResponseDjango() {
   }

   public String getStatus() {
      return status;
   }

   public void setStatus(String status) {
      this.status = status;
   }

   public String getMessage() {
      return message;
   }

   public void setMessage(String message) {
      this.message = message;
   }
}
