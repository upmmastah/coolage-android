package com.example.david.agenda;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.david.agenda.model.Contact;
import com.example.david.agenda.network.NetworkClient;
import com.example.david.agenda.util.ContactDataValidator;
import com.google.gson.Gson;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class EditActivity extends AppCompatActivity {

   public static final int RESULT_CODE = 1;
   String id;

   @Bind(R.id.et_name)
   TextInputEditText name;
   @Bind(R.id.et_surname)
   TextInputEditText surname;
   @Bind(R.id.et_phone)
   TextInputEditText phone;
   @Bind(R.id.et_email)
   TextInputEditText email;
   @Bind(R.id.et_address)
   TextInputEditText address;
   @Bind(R.id.toolbar)
   Toolbar toolbar;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_edit);
      ButterKnife.bind(this);

      setUp();
      setUpToolbar();
   }

   private void setUp() {
      Intent i = getIntent();
      Gson gson = new Gson();
      Contact c = gson.fromJson(i.getExtras().getString("contact"), Contact.class);

      id = c.getId();
      name.setText(c.getName());
      surname.setText(c.getSurname());
      phone.setText(c.getPhone());
      email.setText(c.getEmail());
      address.setText(c.getAddress());
   }

   private void setUpToolbar() {
      setSupportActionBar(toolbar);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      toolbar.setNavigationOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            finish();
         }
      });
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.edit_menu, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int id = item.getItemId();

      switch (id) {
         case R.id.b_save_contact:
            if (ContactDataValidator.canAddContact(name.getText().toString(), email.getText().toString(), this)) {
               updateContact();
            }
            return true;
         default:
            return super.onOptionsItemSelected(item);
      }
   }

   protected void updateContact() {
      final Contact c = new Contact(
            id,
            name.getText().toString(),
            surname.getText().toString(),
            phone.getText().toString(),
            email.getText().toString(),
            address.getText().toString()
      );

      NetworkClient.getService().updateContact(c.getId(), c)
            .subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Action1<ResponseBody>() {
               @Override public void call(ResponseBody response) {
                  Gson gson = new Gson();
                  Intent i = new Intent(EditActivity.this, MainActivity.class);
                  i.putExtra("contact", gson.toJson(c));
                  setResult(RESULT_CODE, i);
                  finish();
               }
            }, new Action1<Throwable>() {
               @Override public void call(Throwable t) {
                  Toast.makeText(EditActivity.this, "No se pudo editar el contacto.", Toast.LENGTH_SHORT).show();
                  t.printStackTrace();
               }
            });
   }
}
