package com.example.david.agenda.network;

import com.example.david.agenda.model.Contact;
import com.example.david.agenda.model.ContactList;
import com.example.david.agenda.model.ResponseDjango;
import com.example.david.agenda.model.ResponsePost;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

public interface NetworkInterface {

   @GET("contacts")
   Observable<ContactList> getContacts();

   @GET("contacts/{id}")
   Observable<Contact> getContactWithId(@Path("id") String id);

   @POST("contacts/")
   Observable<ResponsePost> postContact(@Body Contact contact);

   @PUT("contacts/{id}/")
   Observable<ResponseBody> updateContact(@Path("id") String id, @Body Contact contact);

   @DELETE("contacts/{id}/")
   Observable<ResponseBody> deleteContact(@Path("id") int id);


}



