package com.example.david.agenda;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.agenda.model.Contact;
import com.example.david.agenda.network.NetworkClient;
import com.google.gson.Gson;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class DetailActivity extends AppCompatActivity {

   private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 123;
   private static final int REQUEST_CODE = 1;

   String CONTACT_ID;
   Contact c;

   @Bind(R.id.tv_contact_name)
   TextView tvContactName;
   @Bind(R.id.tv_contact_surname)
   TextView tvContactSurame;
   @Bind(R.id.tv_contact_phone)
   TextView tvContactPhone;
   @Bind(R.id.tv_contact_email)
   TextView tvContactEmail;
   @Bind(R.id.tv_contact_address)
   TextView tvContactAddress;
   @Bind(R.id.toolbar)
   Toolbar toolbar;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_detail);
      ButterKnife.bind(this);

      setUp();
      setUpToolbar();
   }

   private void setUp() {
      Intent i = getIntent();
      CONTACT_ID = i.getExtras().getString("id");
      loadContact();
   }

   private void loadContact() {
      NetworkClient.getService().getContactWithId(CONTACT_ID)
            .subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Action1<Contact>() {
               @Override public void call(Contact response) {
                  c = response;
                  tvContactName.setText(c.getName());
                  tvContactSurame.setText(c.getSurname());
                  tvContactPhone.setText(c.getPhone());
                  tvContactEmail.setText(c.getEmail());
                  tvContactAddress.setText(c.getAddress());
               }
            }, new Action1<Throwable>() {
               @Override public void call(Throwable t) {
                  Toast.makeText(DetailActivity.this, "No se ha podido cargar el contacto.", Toast.LENGTH_SHORT).show();
                  t.printStackTrace();
               }
            });
   }

   private void setUpToolbar() {
      setSupportActionBar(toolbar);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowHomeEnabled(true);

      toolbar.setNavigationOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            finish();
         }
      });
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.contact_detail_menu, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int id = item.getItemId();
      switch (id) {

         case R.id.b_edit_contact:
            Gson gson = new Gson();
            Intent i = new Intent(this, EditActivity.class);
            i.putExtra("contact", gson.toJson(c));
            startActivityForResult(i, REQUEST_CODE);
            break;

         case R.id.b_favorite:
            if (!c.getFavorite()) {
               item.setIcon(R.drawable.ic_heart_white_24dp);
               c.setFavorite(true);
            } else {
               item.setIcon(R.drawable.ic_heart_outline_white_24dp);
               c.setFavorite(false);
            }

            NetworkClient.getService().updateContact(c.getId(), c)
                  .subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribe(new Action1<ResponseBody>() {
                     @Override public void call(ResponseBody response) {
                        Toast.makeText(DetailActivity.this, "Contacto editado.", Toast.LENGTH_SHORT).show();
                     }
                  }, new Action1<Throwable>() {
                     @Override public void call(Throwable t) {
                        Toast.makeText(DetailActivity.this, "No se pudo editar el contacto.", Toast.LENGTH_SHORT).show();
                        t.printStackTrace();
                     }
                  });
            break;

         case R.id.b_delete_contact:

            AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this);
            builder.setTitle("Eliminar contacto");
            builder.setMessage("¿Desea eliminar este contacto?");
            builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int id) {
                  NetworkClient.getService().deleteContact(Integer.parseInt(CONTACT_ID))
                        .subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<ResponseBody>() {
                           @Override public void call(ResponseBody response) {
                              finish();
                           }
                        }, new Action1<Throwable>() {
                           @Override public void call(Throwable t) {
                              Toast.makeText(DetailActivity.this, "No se pudo borrar el contacto.", Toast.LENGTH_SHORT).show();
                              t.printStackTrace();
                           }
                        });
               }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int id) {
                  // User cancelled the dialog
               }
            });
            AlertDialog dialog = builder.create();
            dialog.show();


         default:
            break;
      }
      return super.onOptionsItemSelected(item);
   }

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      if (requestCode == REQUEST_CODE) {
         if (resultCode == NewContactActivity.RESULT_CODE) {
            loadContact();
         }
      }
   }

   @Override
   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
      switch (requestCode) {
         case MY_PERMISSIONS_REQUEST_CALL_PHONE:
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
               // Permission Granted
               Intent intent = new Intent(Intent.ACTION_CALL,
                     Uri.parse("tel:" + tvContactPhone.getText().toString()));
               if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                  return;
               }
               startActivity(intent);
            } else {
               // Permission Denied
               Snackbar.make(findViewById(android.R.id.content),
                     "Permiso WRITE_CONTACTS denegado", Snackbar.LENGTH_SHORT)
                     .show();
            }
            break;
         default:
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
      }
   }

   public void callContact(View view) {
      int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.CALL_PHONE);
      if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
         requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
               MY_PERMISSIONS_REQUEST_CALL_PHONE);
         return;
      }
      Intent intent = new Intent(Intent.ACTION_CALL,
            Uri.parse("tel:" + tvContactPhone.getText().toString()));
      startActivity(intent);

   }
}
