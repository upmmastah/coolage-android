package com.example.david.agenda;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.david.agenda.adapter.ContactAdapter;
import com.example.david.agenda.model.Contact;
import com.example.david.agenda.model.ContactList;
import com.example.david.agenda.network.NetworkClient;
import com.example.david.agenda.util.ContactSearch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

   private static final int REQUEST_CODE = 1;

   @Bind(R.id.contact_list)
   ListView contactList;
   @Bind(R.id.b_new_contact)
   FloatingActionButton fab;
   @Bind(R.id.toolbar)
   Toolbar toolbar;
   @Bind(R.id.tb_search_main)
   android.support.v7.widget.Toolbar searchContainer;
   @Bind(R.id.search_view)
   EditText toolbarSearchView;
   @Bind(R.id.search_clear)
   ImageView searchClearButton;
   @Bind(R.id.search_back)
   ImageView searchBackButton;

   List<Contact> contacts = new ArrayList<>();
   boolean searchFilters[] = new boolean[]{true, false, false, false, false};
   ContactAdapter ca;
   Context c;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      ButterKnife.bind(this);
      c = this.getApplicationContext();
      setUp();
      setUpToolbar();
      contactList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent = new Intent(MainActivity.this, DetailActivity.class);
            intent.putExtra("id", ((Contact) ca.getItem(i)).getId());
            intent.putExtra("id", ((Contact) ca.getItem(i)).getId());
            startActivityForResult(intent, REQUEST_CODE);
            toolbarSearchView.setText("");
         }
      });
   }

   /**
    * Sets up main view
    */
   private void setUp() {
      ca = new ContactAdapter(this, contacts);
      contactList.setAdapter(ca);

      setSupportActionBar(toolbar);

      getContacts(c);
      searchContainer.setVisibility(View.GONE);
   }

   /**
    * Add search to toolbar
    */
   private void setUpToolbar() {
      toolbarSearchView.addTextChangedListener(new TextWatcher() {
         @Override
         public void onTextChanged(CharSequence s, int start, int before, int count) {

            List<Contact> filteredContacts = ContactSearch.searchContacts(contacts, searchFilters, s.toString());
            ca = new ContactAdapter(MainActivity.this, filteredContacts);
            contactList.setAdapter(ca);

            Collections.sort(contacts, new Comparator<Contact>() {
               @Override
               public int compare(Contact contact, Contact c1) {
                  return contact.getName().compareToIgnoreCase(c1.getName());
               }
            });
            ca.notifyDataSetChanged();
         }

         @Override
         public void beforeTextChanged(CharSequence s, int start, int count, int after) {
         }

         @Override
         public void afterTextChanged(Editable s) {
         }
      });
   }

   /**
    * Gets a list of contacts to show in the list
    * @param c
    */
   protected void getContacts(final Context c) {
      NetworkClient.getService().getContacts()
            .subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Action1<ContactList>() {
               @Override public void call(ContactList contactList) {
                  contacts.clear();
                  contacts.addAll(contactList.getResults());

                  Collections.sort(contacts, new Comparator<Contact>() {
                     @Override
                     public int compare(Contact contact, Contact c1) {
                        return contact.getName().compareToIgnoreCase(c1.getName());
                     }
                  });
                  ca.notifyDataSetChanged();
               }
            }, new Action1<Throwable>() {
               @Override public void call(Throwable t) {
                  Toast.makeText(c, "No se han podido cargar contactos.", Toast.LENGTH_SHORT).show();

                  t.printStackTrace();
               }
            });
   }

   @Override
   protected void onResume() {
      super.onResume();
      searchContainer.setVisibility(View.GONE);
      toolbar.setVisibility(View.VISIBLE);
      contactList = (ListView) findViewById(R.id.contact_list);
      ca = new ContactAdapter(this, contacts);
      contactList.setAdapter(ca);
      getContacts(c);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.main_menu, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int id = item.getItemId();
      switch (id) {
         case R.id.b_search_contact:
            toolbar.setVisibility(View.GONE);
            searchContainer.setVisibility(View.VISIBLE);
            break;
         default:
      }
      return super.onOptionsItemSelected(item);
   }

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {

      super.onActivityResult(requestCode, resultCode, data);
      if (requestCode == REQUEST_CODE) {
         if (resultCode == NewContactActivity.RESULT_CODE) {
            getContacts(this.findViewById(android.R.id.content).getContext());
         }
      }
   }

   public void newContact(View view) {
      Intent intent = new Intent(this, NewContactActivity.class);
      startActivityForResult(intent, REQUEST_CODE);
   }

   public void cancelSearch(View view) {
      if (view != null) {
         InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
         imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
      }

      toolbarSearchView.setText("");
      toolbar.setVisibility(View.VISIBLE);
      searchContainer.setVisibility(View.GONE);
      getContacts(c);
   }

   /**
    * Clears text on search bar
    * @param view
    */
   public void clearSearch(View view) {
      toolbarSearchView.setText("");
   }

   /**
    * Filters contacts for search
    * @param view
    */
   public void filter(View view) {
      PopupMenu popup = new PopupMenu(MainActivity.this, view);
      MenuInflater inflater = popup.getMenuInflater();
      inflater.inflate(R.menu.filter_menu, popup.getMenu());

      popup.getMenu().findItem(R.id.f_name).setChecked(searchFilters[0]);
      popup.getMenu().findItem(R.id.f_surname).setChecked(searchFilters[1]);
      popup.getMenu().findItem(R.id.f_phone).setChecked(searchFilters[2]);
      popup.getMenu().findItem(R.id.f_email).setChecked(searchFilters[3]);
      popup.getMenu().findItem(R.id.f_address).setChecked(searchFilters[4]);

      popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

         @Override
         public boolean onMenuItemClick(MenuItem item) {
            int id = item.getItemId();
            switch (id) {
               case R.id.f_name:
                  searchFilters[0] = !item.isChecked();
                  break;
               case R.id.f_surname:
                  searchFilters[1] = !item.isChecked();
                  break;
               case R.id.f_phone:
                  searchFilters[2] = !item.isChecked();
                  break;
               case R.id.f_email:
                  searchFilters[3] = !item.isChecked();
                  break;
               case R.id.f_address:
                  searchFilters[4] = !item.isChecked();
                  break;
               default:
            }

            item.setChecked(!item.isChecked());
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
            item.setActionView(new View(c));
            MenuItemCompat.setOnActionExpandListener(item,
                  new MenuItemCompat.OnActionExpandListener() {
                     @Override
                     public boolean onMenuItemActionExpand(MenuItem item) {
                        return false;
                     }

                     @Override
                     public boolean onMenuItemActionCollapse(MenuItem item) {
                        return false;
                     }
                  });

            return false;
         }
      });

      popup.show();
   }
}
