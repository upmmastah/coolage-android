package com.example.david.agenda.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.david.agenda.R;
import com.example.david.agenda.model.Contact;

import java.util.List;

public class ContactAdapter extends BaseAdapter {

   private Context context;
   private List<Contact> contacts;

   public ContactAdapter(Context context, List<Contact> contacts) {
      this.context = context;
      this.contacts = contacts;
   }

   @Override
   public int getCount() {
      return this.contacts.size();
   }

   @Override
   public Object getItem(int position) {
      return this.contacts.get(position);
   }

   @Override
   public long getItemId(int position) {
      return position;
   }

   @Override
   public View getView(int position, View view, ViewGroup parent) {

      View rowView = view;

      if (view == null) {
         LayoutInflater inflater = (LayoutInflater) context
               .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         rowView = inflater.inflate(R.layout.contact, parent, false);
      }

      TextView name = (TextView) rowView.findViewById(R.id.name);
      TextView phone = (TextView) rowView.findViewById(R.id.phone);
      ImageView favorite = (ImageView) rowView.findViewById(R.id.favorite);

      Contact contact = this.contacts.get(position);
      name.setText(contact.getName());
      phone.setText(contact.getPhone());
      if (contact.getFavorite()) {
         favorite.setVisibility(View.VISIBLE);
      }

      return rowView;

   }

}