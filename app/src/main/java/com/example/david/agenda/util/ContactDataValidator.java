package com.example.david.agenda.util;

import android.app.Activity;
import android.support.design.widget.Snackbar;

import com.example.david.agenda.R;

public class ContactDataValidator {

   private static final String EMAIL_PATTERN =
         "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
               + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

   /**
    * Validate if name is long enough
    * @param name
    * @return
    */
   public static boolean validateNameLength(String name) {
      return name.length() >= 3;
   }

   /**
    * Validate if name is empty
    * @param name
    * @return
    */
   public static boolean validateNameEmpty(String name) {
      return !name.equals("");
   }

   /**
    * Validate email if it is not empty
    * @param email String
    * @return true if email is empty
    */
   public static boolean validateEmail(String email) {
      return email.equals("") || email.matches(EMAIL_PATTERN);
   }

   /**
    * Checks if fields are valid and shows a message from those who aren't
    * @param name String
    * @param email String
    * @param a Activity
    * @return true if valid
    */
   public static boolean canAddContact(String name, String email, Activity a) {

      if (!validateNameEmpty(name)) {
         Snackbar.make(a.findViewById(R.id.activity_edit),
               "Es necesario rellenar al menos el nombre del contacto.",
               Snackbar.LENGTH_SHORT).show();
         return false;
      }
      if (!validateNameLength(name)) {
         Snackbar.make(a.findViewById(R.id.activity_edit),
               "El nombre tiene que contener al menos 3 caracteres.",
               Snackbar.LENGTH_SHORT).show();
         return false;
      }
      if (!validateEmail(email)) {
         Snackbar.make(a.findViewById(R.id.activity_edit),
               "Introduce un correo válido.",
               Snackbar.LENGTH_SHORT).show();
         return false;
      }

      return true;
   }

}
