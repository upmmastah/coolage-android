package com.example.david.agenda;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.example.david.agenda.model.Contact;
import com.example.david.agenda.model.ResponsePost;
import com.example.david.agenda.network.NetworkClient;
import com.example.david.agenda.util.ContactDataValidator;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class NewContactActivity extends AppCompatActivity {
   public static final int RESULT_CODE = 1;

   @Bind(R.id.et_name)
   TextInputEditText name;
   @Bind(R.id.et_surname)
   TextInputEditText surname;
   @Bind(R.id.et_phone)
   TextInputEditText phone;
   @Bind(R.id.et_email)
   TextInputEditText email;
   @Bind(R.id.et_address)
   TextInputEditText address;
   @Bind(R.id.toolbar)
   Toolbar toolbar;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_edit);
      ButterKnife.bind(this);

      setUpToolbar();
   }

   private void setUpToolbar() {
      setSupportActionBar(toolbar);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      toolbar.setNavigationOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            finish();
         }
      });
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.new_contact_menu, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      int id = item.getItemId();

      switch (id) {
         case R.id.b_new_contact:
            View view = this.getCurrentFocus();
            if (view != null) {
               InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
               imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            if (ContactDataValidator.canAddContact(name.getText().toString(), email.getText().toString(), this)) {
               newContact();
            }
            return true;
         default:
            return super.onOptionsItemSelected(item);
      }
   }

   protected void newContact() {
      Contact c = new Contact(
            null,
            name.getText().toString(),
            surname.getText().toString(),
            phone.getText().toString(),
            email.getText().toString(),
            address.getText().toString()
      );


      NetworkClient.getService().postContact(c)
            .subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Action1<ResponsePost>() {
               @Override public void call(ResponsePost response) {
                  Snackbar.make(findViewById(android.R.id.content), "Request successful", Snackbar.LENGTH_SHORT).show();
                  setResult(RESULT_CODE);
                  finish();
               }
            }, new Action1<Throwable>() {
               @Override public void call(Throwable t) {
                  Snackbar.make(findViewById(android.R.id.content), "Request failed", Snackbar.LENGTH_SHORT).show();
                  t.printStackTrace();
               }
            });
   }
}
